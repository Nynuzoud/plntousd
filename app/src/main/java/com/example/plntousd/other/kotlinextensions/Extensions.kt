package com.example.plntousd.other.kotlinextensions

import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import java.math.BigDecimal
import java.math.RoundingMode

fun View.showSnackbar(@StringRes message: Int, @StringRes buttonText: Int, length: Int, function: () -> Unit) {
    val snackbar = Snackbar
        .make(this, message, length)

    snackbar
        .setAction(buttonText) {
            function()
            snackbar.dismiss()
        }
        .show()
}

fun Double.setScaleToString(scale: Int, roundingMode: RoundingMode): String {
    return BigDecimal(this).setScale(scale, roundingMode).toString()
}