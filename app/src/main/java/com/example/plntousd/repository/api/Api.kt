package com.example.plntousd.repository.api

import com.example.plntousd.data.dto.RatesDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("/latest")
    fun getLatestRates(@Query("base") base: String? = RatesDTO.DEFAULT_BASE.name): Single<RatesDTO>
}