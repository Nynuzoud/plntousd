package com.example.plntousd.repository.rates

import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.repository.api.rates.CurrenciesEnum
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

interface RatesRepository {

    fun getRatesWithInterval(
        intervalValue: Long,
        timeUnit: TimeUnit,
        base: CurrenciesEnum
    ): Observable<RatesDTO>
}