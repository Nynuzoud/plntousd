package com.example.plntousd.repository.api.rates

enum class CurrenciesEnum {

    PLN,
    USD
}