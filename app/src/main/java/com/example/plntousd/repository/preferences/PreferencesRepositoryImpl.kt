package com.example.plntousd.repository.preferences

import android.content.SharedPreferences
import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.repository.api.rates.CurrenciesEnum

class PreferencesRepositoryImpl(private val preferences: SharedPreferences) : PreferencesRepository {

    companion object {

        private const val LATEST_UPDATE_DATE = "LATEST_UPDATE_DATE"
    }

    /**
     * Saves latest rates
     * @param [rates]
     */
    override fun saveLatestRates(rates: RatesDTO) {
        with(rates) {
            putValue(LATEST_UPDATE_DATE to date)
            putMap(rates.rates)
        }
    }

    /**
     * Retrieves latest cached rates
     * @return [com.example.plntousd.data.dto.RatesDTO]
     */
    override fun getLatestRates(): RatesDTO =
        RatesDTO(
            base = RatesDTO.DEFAULT_BASE.name,
            date = preferences.getString(LATEST_UPDATE_DATE, "") ?: "",
            rates = getMap()
        )

    private fun putValue(pair: Pair<String, Any?>) = with(preferences.edit()) {
        val key = pair.first

        when (val value = pair.second ?: return@with) {
            is String -> putString(key, value)
            is Int -> putInt(key, value)
            is Boolean -> putBoolean(key, value)
            is Long -> putLong(key, value)
            is Float -> putFloat(key, value)
            else -> error("Only primitives types can be stored in Shared Preferences")
        }

        apply()
    }

    private fun putMap(map: Map<String, Double>) = with(preferences.edit()) {
        map.forEach { (currenciesEnum, value) ->
            putString(currenciesEnum, value.toString())
        }
        apply()
    }

    private fun getMap(): Map<String, Double> {
        val map = LinkedHashMap<String, Double>(CurrenciesEnum.values().size, 1f)
        CurrenciesEnum.values().forEach {
            map[it.name] = preferences.getString(it.name, null)?.toDouble() ?: RatesDTO.DEFAULT_MULTIPLIER
        }
        return map
    }
}