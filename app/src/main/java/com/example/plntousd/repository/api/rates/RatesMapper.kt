package com.example.plntousd.repository.api.rates
//
//import com.example.plntousd.data.dto.RatesDTO
//import com.example.plntousd.other.Utils
//import com.example.plntousd.data.rates.Rates
//import io.reactivex.functions.Function
//
///**
// * Converts [RatesDTO] to [Rates] just to prepare data from Internet for UI
// */
//class RatesMapper : Function<RatesDTO, Rates> {
//    override fun apply(t: RatesDTO): Rates {
//        return Rates(
//            base = t.base,
//            date = Utils.parseDate(t.date),
//            ratesEnumMap = Utils.roundAllCurrenciesValues(t.ratesMap)
//        )
//    }
//}