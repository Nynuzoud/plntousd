package com.example.plntousd.repository.preferences

import com.example.plntousd.data.dto.RatesDTO


interface PreferencesRepository {

    fun saveLatestRates(rates: RatesDTO)
    fun getLatestRates(): RatesDTO
}