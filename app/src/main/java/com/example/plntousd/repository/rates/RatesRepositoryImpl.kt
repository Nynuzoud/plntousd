package com.example.plntousd.repository.rates

import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.repository.api.Api
import com.example.plntousd.repository.api.rates.CurrenciesEnum
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Repository that handles [com.example.plntousd.repository.api.rates.RatesDTO]
 */

class RatesRepositoryImpl(private val api: Api) : RatesRepository {

    private val DEFAULT_START_VAL= 1L

    override fun getRatesWithInterval(
        intervalValue: Long,
        timeUnit: TimeUnit,
        base: CurrenciesEnum
    ): Observable<RatesDTO> {
        return Observable
            .interval(intervalValue, timeUnit, Schedulers.computation())
            .startWith(DEFAULT_START_VAL)
            .switchMapSingle {
                api.getLatestRates(base.name)
            }
            .subscribeOn(Schedulers.io())
    }
}