package com.example.plntousd.di.modules

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.example.plntousd.BuildConfig
import com.example.plntousd.repository.api.Api
import com.example.plntousd.repository.preferences.PreferencesRepository
import com.example.plntousd.repository.preferences.PreferencesRepositoryImpl
import com.example.plntousd.repository.rates.RatesRepository
import com.example.plntousd.repository.rates.RatesRepositoryImpl
import com.example.plntousd.ui.usecases.CalculateCurrenciesUseCase
import com.example.plntousd.ui.usecases.CalculateCurrenciesUseCaseImpl
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    ///////////////////API//////////////////////

    @Singleton
    @Provides
    fun provideApiInterface(): Api {
        val logging = HttpLoggingInterceptor()

        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.HEADERS
        }

        val client = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build()

        val gson = GsonBuilder()
                .create()

        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
                .create(Api::class.java)
    }

    @Singleton
    @Provides
    fun providePreferences(application: Application): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(application.applicationContext)

    ///////////////////REPOSITORIES//////////////////////

    @Provides
    @Singleton
    fun provideRatesRepository(api: Api): RatesRepository =
        RatesRepositoryImpl(api)

    @Provides
    @Singleton
    fun providePreferencesRepository(preferences: SharedPreferences): PreferencesRepository =
        PreferencesRepositoryImpl(preferences)

    //////////////////USE CASES ////////////////////////////
    @Provides
    @Singleton
    fun provideCalculateCurrenciesUseCase(): CalculateCurrenciesUseCase = CalculateCurrenciesUseCaseImpl()
}