package com.example.plntousd.di

import android.app.Application
import com.example.plntousd.di.components.AppComponent
import com.example.plntousd.di.components.DaggerAppComponent
import com.example.plntousd.di.modules.AppModule


object ComponentsHolder {

    lateinit var applicationComponent: AppComponent

    fun init(application: Application) {

        applicationComponent = DaggerAppComponent.builder()
                .appModule(AppModule(application))
                .build()
    }
}