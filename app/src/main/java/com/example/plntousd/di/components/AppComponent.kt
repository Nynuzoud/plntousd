package com.example.plntousd.di.components

import android.app.Application
import com.example.plntousd.di.modules.AppModule
import com.example.plntousd.repository.api.Api
import com.example.plntousd.ui.MainActivity
import com.example.plntousd.ui.MainViewModel
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {

    val application: Application
    val api: Api

    fun inject(mainActivity: MainActivity)
    fun inject(mainViewModel: MainViewModel)
}
