package com.example.plntousd.ui.usecases

import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.other.kotlinextensions.setScaleToString
import com.example.plntousd.repository.api.rates.CurrenciesEnum
import java.math.BigDecimal

class CalculateCurrenciesUseCaseImpl : CalculateCurrenciesUseCase {

    override var rates: RatesDTO? = null

    private val _convertedEnumMap = LinkedHashMap<String, Double>(CurrenciesEnum.values().size, 1f)
    override val convertedEnumMap: LinkedHashMap<String, Double>
        get() = _convertedEnumMap

    private var _baseCurrency: CurrenciesEnum = RatesDTO.DEFAULT_BASE
    override var baseCurrency: CurrenciesEnum
        set(value) {
            _baseCurrency = value
            _baseMultiplier = convertedEnumMap[value.name] ?: RatesDTO.DEFAULT_MULTIPLIER
            rates = generatePredictedRates()
        }
        get() = _baseCurrency

    private var _baseMultiplier: Double = 0.0
    override var baseMultiplier: String?
        set(value) {
            if (!value.isNullOrEmpty()) {
                _baseMultiplier = value.toDouble()
            } else {
                RatesDTO.DEFAULT_MULTIPLIER
            }
        }
        get() = _baseMultiplier.toString()

    override fun getConvertedValue(value: String?, currency: CurrenciesEnum): String {
        baseMultiplier = value
        updateConvertedRates()
        return when (baseCurrency) {
            CurrenciesEnum.PLN -> {
                (convertedEnumMap[CurrenciesEnum.USD.name]
                    ?: RatesDTO.DEFAULT_MULTIPLIER)
            }
            CurrenciesEnum.USD -> {
                (convertedEnumMap[CurrenciesEnum.PLN.name]
                    ?: RatesDTO.DEFAULT_MULTIPLIER)
            }
        }.setScaleToString(RatesDTO.DEFAULT_ROUND_SCALE, RatesDTO.DEFAULT_ROUNDING)
    }

    /**
     * Updates default rates by multiplying base value on user value
     */
    override fun updateConvertedRates() {
        convertedEnumMap[baseCurrency.name] = _baseMultiplier

        rates?.rates?.forEach { (key, value) ->
            if (key != baseCurrency.name) {
                convertedEnumMap[key] =
                    BigDecimal(value)
                        .multiply(BigDecimal(_baseMultiplier))
                        .setScale(RatesDTO.DEFAULT_ROUND_SCALE, RatesDTO.DEFAULT_ROUNDING)
                        .toDouble()
            }
        }
    }

    /**
     * Generates expected rates to make a fast data replacement after user's changes
     * This rates will be updated with actual data after server response
     * @return [com.example.plntousd.data.dto.RatesDTO]
     */
    private fun generatePredictedRates(): RatesDTO {
        val ratesMap = LinkedHashMap<String, Double>(CurrenciesEnum.values().size, 1f)
        val newCurrencyRate = rates?.rates?.get(baseCurrency.name) ?: RatesDTO.DEFAULT_MULTIPLIER
        CurrenciesEnum.values().forEach {
            if (it != baseCurrency) {
                val oldRateValue = rates?.rates?.get(it.name) ?: RatesDTO.DEFAULT_MULTIPLIER

                ratesMap[it.name] = BigDecimal(oldRateValue)
                    .divide(
                        BigDecimal(newCurrencyRate),
                        RatesDTO.DEFAULT_ROUND_SCALE,
                        RatesDTO.DEFAULT_ROUNDING
                    )
                    .setScale(RatesDTO.DEFAULT_ROUND_SCALE, RatesDTO.DEFAULT_ROUNDING)
                    .toDouble()
            }
        }

        return RatesDTO(
            base = baseCurrency.name,
            date = rates?.date ?: "",
            rates = ratesMap
        )
    }

}