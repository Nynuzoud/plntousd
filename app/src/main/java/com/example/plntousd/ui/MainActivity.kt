package com.example.plntousd.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.plntousd.R
import com.example.plntousd.databinding.ActivityMainBinding
import com.example.plntousd.di.ComponentsHolder
import com.example.plntousd.other.kotlinextensions.showSnackbar
import com.example.plntousd.repository.api.rates.CurrenciesEnum
import com.example.plntousd.viewmodel.ViewModelFactory
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        ComponentsHolder.applicationComponent.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        binding.mainViewModel = viewModel

        viewModel.errorsLiveData.observe(this, Observer {
            root.showSnackbar(it, R.string.retry, Snackbar.LENGTH_INDEFINITE) {
                viewModel.startGettingRates()
            }
        })

        setupListeners()
        viewModel.startGettingRates()
    }

    private fun setupListeners() {
        currency_edit_pln.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                viewModel.setCurrentBaseCurrency(CurrenciesEnum.PLN)
            }
        }

        currency_edit_usd.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                viewModel.setCurrentBaseCurrency(CurrenciesEnum.USD)
            }
        }
    }
}
