package com.example.plntousd.ui

import android.app.Application
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.example.plntousd.R
import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.di.components.AppComponent
import com.example.plntousd.other.kotlinextensions.setScaleToString
import com.example.plntousd.repository.api.rates.CurrenciesEnum
import com.example.plntousd.repository.preferences.PreferencesRepository
import com.example.plntousd.repository.rates.RatesRepository
import com.example.plntousd.ui.usecases.CalculateCurrenciesUseCase
import com.example.plntousd.viewmodel.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.io.IOException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(
    applicationComponent: AppComponent,
    private val ratesRepository: RatesRepository,
    private val preferencesRepository: PreferencesRepository,
    private val application: Application,
    private val calculateCurrenciesUseCase: CalculateCurrenciesUseCase
) : BaseViewModel() {

    val errorsLiveData = MutableLiveData<Int>()

    val plnValue: ObservableField<String> = ObservableField(RatesDTO.DEFAULT_MULTIPLIER.toString())
    val usdValue: ObservableField<String> = ObservableField(RatesDTO.DEFAULT_MULTIPLIER.toString())
    val lastUpdate: ObservableField<String> = ObservableField("")

    private var ratesDisposable: Disposable? = null

    init {
        applicationComponent.inject(this)
        plnValue.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (calculateCurrenciesUseCase.baseCurrency == CurrenciesEnum.PLN) {
                    usdValue.set(
                        calculateCurrenciesUseCase.getConvertedValue(
                            plnValue.get(),
                            CurrenciesEnum.PLN
                        )
                    )
                }
            }
        })
        usdValue.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (calculateCurrenciesUseCase.baseCurrency == CurrenciesEnum.USD) {
                    plnValue.set(
                        calculateCurrenciesUseCase.getConvertedValue(
                            usdValue.get(),
                            CurrenciesEnum.USD
                        )
                    )
                }
            }
        })
        calculateCurrenciesUseCase.rates = preferencesRepository.getLatestRates()
        calculateCurrenciesUseCase.updateConvertedRates()
        restoreData()
    }

    /**
     * Restores latest default rates from [SharedPreferences]
     */
    private fun restoreData() {
        with(calculateCurrenciesUseCase.rates) {
            if (!this?.date.isNullOrEmpty()) {
                lastUpdate.set(
                    application.resources.getString(
                        R.string.last_update_date,
                        this?.date
                    )
                )
            }
            if (this?.rates?.get(CurrenciesEnum.PLN.name) != null) {
                plnValue.set(
                    this.rates[CurrenciesEnum.PLN.name]?.setScaleToString(
                        RatesDTO.DEFAULT_ROUND_SCALE,
                        RatesDTO.DEFAULT_ROUNDING
                    )
                )
            }
            if (this?.rates?.get(CurrenciesEnum.USD.name) != null) {
                usdValue.set(
                    this.rates[CurrenciesEnum.USD.name]?.setScaleToString(
                        RatesDTO.DEFAULT_ROUND_SCALE,
                        RatesDTO.DEFAULT_ROUNDING
                    )
                )
            }
        }
    }

    /**
     * Sets new default base currency
     * @param currency - new default value
     */
    fun setCurrentBaseCurrency(currency: CurrenciesEnum) {
        calculateCurrenciesUseCase.baseCurrency = currency
    }

    /**
     * Gets rate updates every minute till [MainViewModel] is alive.
     * Then saves latest rates to [SharedPreferences]
     * Send an error message to the view if error happens
     */
    fun startGettingRates() {
        unsubscribe(ratesDisposable)
        ratesDisposable = ratesRepository.getRatesWithInterval(
            1,
            TimeUnit.MINUTES,
            calculateCurrenciesUseCase.baseCurrency
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                calculateCurrenciesUseCase.rates = it
                lastUpdate.set(application.resources.getString(R.string.last_update_date, it.date))
                preferencesRepository.saveLatestRates(it)
                calculateCurrenciesUseCase.updateConvertedRates()

                usdValue.set(
                    (calculateCurrenciesUseCase.convertedEnumMap[CurrenciesEnum.USD.name]
                        ?: RatesDTO.DEFAULT_MULTIPLIER).setScaleToString(
                        RatesDTO.DEFAULT_ROUND_SCALE,
                        RatesDTO.DEFAULT_ROUNDING
                    )
                )
                plnValue.set(
                    (calculateCurrenciesUseCase.convertedEnumMap[CurrenciesEnum.PLN.name]
                        ?: RatesDTO.DEFAULT_MULTIPLIER).setScaleToString(
                        RatesDTO.DEFAULT_ROUND_SCALE,
                        RatesDTO.DEFAULT_ROUNDING
                    )
                )
            }, {
                when (it) {
                    is UnknownHostException -> errorsLiveData.value = R.string.network_error
                    is IOException -> {
                    } //do nothing
                    else -> errorsLiveData.value = R.string.unknown_error
                }
                Timber.d(it)
            })


        subscribe(ratesDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        clearSubscriptions()
    }
}