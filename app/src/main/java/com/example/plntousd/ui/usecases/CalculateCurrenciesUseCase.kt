package com.example.plntousd.ui.usecases

import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.repository.api.rates.CurrenciesEnum

interface CalculateCurrenciesUseCase {

    fun getConvertedValue(value: String?, currency: CurrenciesEnum): String

    fun updateConvertedRates()

    var baseMultiplier: String?
    var rates: RatesDTO?
    val convertedEnumMap: LinkedHashMap<String, Double>
    var baseCurrency: CurrenciesEnum
}