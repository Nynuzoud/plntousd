package com.example.plntousd.data.dto

import com.example.plntousd.repository.api.rates.CurrenciesEnum
import java.math.RoundingMode

data class RatesDTO(

    var rates: Map<String, Double>,
    var base: String,
    var date: String? = null
) {

    companion object {
        const val DEFAULT_MULTIPLIER = 1.0
        const val DEFAULT_ROUND_SCALE = 2
        val DEFAULT_ROUNDING = RoundingMode.HALF_UP
        val DEFAULT_BASE = CurrenciesEnum.PLN
    }
}