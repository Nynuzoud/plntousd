package com.example.plntousd

import android.app.Application
import com.example.plntousd.di.ComponentsHolder
import com.example.plntousd.other.TimberDebugTree
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initTimber()
        initDagger()
    }

    private fun initTimber() {
        Timber.plant(TimberDebugTree())
    }

    private fun initDagger() {
        ComponentsHolder.init(this)
    }
}