package com.example.plntousd.repo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.plntousd.BaseTest
import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.repository.api.Api
import com.example.plntousd.repository.api.rates.CurrenciesEnum
import com.example.plntousd.repository.rates.RatesRepository
import com.example.plntousd.repository.rates.RatesRepositoryImpl
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.Mockito.mock
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class RatesRepositoryTest : BaseTest() {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val DEFAULT_DATE = "2019-09-04"

    private val api: Api = mock(Api::class.java)
    private val repo: RatesRepository = RatesRepositoryImpl(api)

    private lateinit var ratesMap: MutableMap<String, Double>
    private lateinit var ratesDTO: RatesDTO

    @Before
    fun init() {

        ratesMap = LinkedHashMap()
        ratesMap["PLN"] = 2.0

        ratesDTO = RatesDTO(
            rates = ratesMap,
            base = "USD",
            date = DEFAULT_DATE
        )
    }

    @Test
    fun getRates() {

        Mockito
            .doReturn(Single.just(ratesDTO))
            .`when`(api)
            .getLatestRates(CurrenciesEnum.USD.name)

        val testObserver = repo.getRatesWithInterval(1, TimeUnit.MINUTES, CurrenciesEnum.USD)
            .test()
            .assertValueCount(1)
            .assertValue(ratesDTO)
            .assertNoErrors()
            .assertNotComplete()

        testScheduler.advanceTimeTo(1, TimeUnit.SECONDS)

        testObserver
            .assertNoErrors()
            .dispose()
    }

}