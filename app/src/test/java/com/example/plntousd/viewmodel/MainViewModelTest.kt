package com.example.plntousd.viewmodel

import android.app.Application
import android.content.res.Resources
import com.example.plntousd.BaseTest
import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.di.components.AppComponent
import com.example.plntousd.repository.api.rates.CurrenciesEnum
import com.example.plntousd.repository.preferences.PreferencesRepository
import com.example.plntousd.repository.rates.RatesRepository
import com.example.plntousd.ui.MainViewModel
import com.example.plntousd.ui.usecases.CalculateCurrenciesUseCase
import com.example.plntousd.ui.usecases.CalculateCurrenciesUseCaseImpl
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.Mockito.mock
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class MainViewModelTest : BaseTest() {

    private val DEFAULT_DATE = "2019-09-04"

    private val appComponent: AppComponent = mock(AppComponent::class.java)
    private val ratesRepository: RatesRepository = mock(RatesRepository::class.java)
    private val preferencesRepository: PreferencesRepository =
        mock(PreferencesRepository::class.java)
    private val application: Application = mock(Application::class.java)
    private val useCase: CalculateCurrenciesUseCase = Mockito.spy(CalculateCurrenciesUseCaseImpl())

    private val resources: Resources = mock(Resources::class.java)

    private lateinit var viewModel: MainViewModel

    private lateinit var ratesMap: MutableMap<String, Double>
    private lateinit var ratesDTO: RatesDTO

    @Before
    fun init() {

        Mockito.doReturn(resources)
            .`when`(application)
            .resources

        Mockito.doReturn("Last update: $DEFAULT_DATE")
            .`when`(resources)
            .getString(Mockito.anyInt(), Mockito.eq(DEFAULT_DATE))

        ratesMap = LinkedHashMap()
        ratesMap["PLN"] = 2.0

        ratesDTO = RatesDTO(
            rates = ratesMap,
            base = "USD",
            date = DEFAULT_DATE
        )

        Mockito.`when`(preferencesRepository.getLatestRates())
            .thenReturn(ratesDTO)

        useCase.rates = ratesDTO

        viewModel = MainViewModel(
            appComponent,
            ratesRepository,
            preferencesRepository,
            application,
            useCase
        )
    }

    @Test
    fun initTest() {

        Assert.assertEquals("Last update: $DEFAULT_DATE", viewModel.lastUpdate.get())
        Assert.assertEquals("2.00", viewModel.plnValue.get())
        Assert.assertEquals("1.00", viewModel.usdValue.get())
    }

    @Test
    fun startGettingRates() {
        Mockito
            .doReturn(Observable.never<RatesDTO>()
                .startWith(ratesDTO))
            .`when`(ratesRepository)
            .getRatesWithInterval(1, TimeUnit.MINUTES, CurrenciesEnum.USD)

        useCase.baseCurrency = CurrenciesEnum.USD

        viewModel.startGettingRates()

        Assert.assertEquals("Last update: $DEFAULT_DATE", viewModel.lastUpdate.get())

        Mockito.verify(preferencesRepository)
            .saveLatestRates(ratesDTO)

        Assert.assertEquals("2.00", viewModel.plnValue.get())
        Assert.assertEquals("1.00", viewModel.usdValue.get())
    }

}