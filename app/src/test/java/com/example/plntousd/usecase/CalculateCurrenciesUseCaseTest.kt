package com.example.plntousd.usecase

import com.example.plntousd.BaseTest
import com.example.plntousd.data.dto.RatesDTO
import com.example.plntousd.repository.api.rates.CurrenciesEnum
import com.example.plntousd.ui.usecases.CalculateCurrenciesUseCaseImpl
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CalculateCurrenciesUseCaseTest : BaseTest() {

    private val DEFAULT_DATE = "2019-09-04"

    private val useCase = CalculateCurrenciesUseCaseImpl()

    private lateinit var ratesUsdBaseMap: MutableMap<String, Double>
    private lateinit var ratesUsdBaseDTO: RatesDTO

    private lateinit var ratesPlnBaseMap: MutableMap<String, Double>
    private lateinit var ratesPlnBaseDTO: RatesDTO

    @Before
    fun init() {
        //1 USD EQUALS 4 PLN
        ratesUsdBaseMap = LinkedHashMap()
        ratesUsdBaseMap["PLN"] = 2.0

        ratesUsdBaseDTO = RatesDTO(
            rates = ratesUsdBaseMap,
            base = "USD",
            date = DEFAULT_DATE
        )

        ratesPlnBaseMap = LinkedHashMap()
        ratesPlnBaseMap["USD"] = 0.5

        ratesPlnBaseDTO = RatesDTO(
            rates = ratesPlnBaseMap,
            base = "PLN",
            date = DEFAULT_DATE
        )

        useCase.rates = ratesUsdBaseDTO
    }

    @Test
    fun setBaseCurrency() {
        useCase.baseCurrency = CurrenciesEnum.PLN

        Assert.assertEquals(CurrenciesEnum.PLN, useCase.baseCurrency)
        Assert.assertEquals(RatesDTO.DEFAULT_MULTIPLIER.toString(), useCase.baseMultiplier)
        Assert.assertEquals(ratesPlnBaseDTO, useCase.rates)
    }

    @Test
    fun getConvertedValue() {
        useCase.baseCurrency = CurrenciesEnum.USD
        Assert.assertEquals("4.00", useCase.getConvertedValue("2", CurrenciesEnum.USD))
    }
}